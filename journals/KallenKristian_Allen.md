06/26/23
Today we received our repo and discussed the remaining items that we needed to collectively decide upon. After adding those as well as our tasks into our Trello, set up our repo. Then, we decided to focus on the JWTDown exploration to be ready to go for auth first thing tomorrow. No blockers.

06/27/23
Today we completed auth, tomorrow we will decide if we want to personalize right away or come back if there is time, then continue work on the backend. No blockers.

06/28/23
Today we discussed some backend goals for the week and decided we will focus solely on meeting requirements before adding on any personalization or stretch goals. We continued working on the backend, focusing on our dogs pydantic model and getting data into the database. We ran into some issues with the foreign key and were able to solve a few but still have some errors as blockers. Happy Birthday Dre!

06/29/23
This morning we successfully fixed the errors from yesterday. Starting the day with no blockers. We are continuing work on the backend today and made a goal to finish it tomorrow. 

06/30/23
We accomplished a lot of work yesterday on the backend, but ended on debugging our update query. We are almost done with the backend and hoping to be finished today. We will be picking up with where left off yesterday.
